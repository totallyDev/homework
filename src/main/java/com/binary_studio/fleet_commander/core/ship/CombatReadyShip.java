package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldEHP;

	private PositiveInteger hullEHP;

	private PositiveInteger powergridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger currentSpeed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	private final PositiveInteger initialShieldEHP;

	private final PositiveInteger initialHullEHP;

	private final PositiveInteger initialCapacitorAmount;

	CombatReadyShip(String name, PositiveInteger shieldEHP, PositiveInteger hullEHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger currentSpeed,
			PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {

		this.name = name;
		this.shieldEHP = shieldEHP;
		this.hullEHP = hullEHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.currentSpeed = currentSpeed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.initialShieldEHP = shieldEHP;
		this.initialHullEHP = hullEHP;
		this.initialCapacitorAmount = capacitorAmount;
	}

	@Override
	public void endTurn() {
		PositiveInteger finalCapacitorAmount = PositiveInteger
				.of(this.capacitorRechargeRate.value() + this.capacitorAmount.value());
		this.capacitorAmount = PositiveInteger.of(finalCapacitorAmount.value() > this.initialCapacitorAmount.value()
				? this.initialCapacitorAmount.value() - this.capacitorAmount.value() : finalCapacitorAmount.value());
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.currentSpeed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();

		}
		else {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			PositiveInteger damage = this.attackSubsystem.attack(target);
			AttackAction attackAction = new AttackAction(damage, this, target, this.attackSubsystem);
			return Optional.of(attackAction);
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		if (reducedAttack.damage.value() < this.shieldEHP.value()) {
			this.shieldEHP = PositiveInteger.of(this.shieldEHP.value() - reducedAttack.damage.value());
			return new AttackResult.DamageRecived(attack.weapon, reducedAttack.damage, this);
		}
		else {
			PositiveInteger restDamage = PositiveInteger.of(reducedAttack.damage.value() - this.shieldEHP.value());
			this.shieldEHP = PositiveInteger.of(0);
			if (restDamage.value() < this.hullEHP.value()) {
				this.hullEHP = PositiveInteger.of(this.hullEHP.value() - restDamage.value());
				return new AttackResult.DamageRecived(attack.weapon, reducedAttack.damage, this);
			}
			else {
				return new AttackResult.Destroyed();
			}
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {
			PositiveInteger finalShieldRegeneration = PositiveInteger.of(this.shieldEHP.value());
			PositiveInteger finalHullRegeneration = PositiveInteger.of(this.hullEHP.value());

			if (finalShieldRegeneration.value() > this.initialShieldEHP.value()) {
				PositiveInteger shieldRegenerationDiff = PositiveInteger
						.of(this.initialShieldEHP.value() - this.shieldEHP.value());
				this.defenciveSubsystem.setShieldRegeneration(shieldRegenerationDiff);

			}
			else if (finalHullRegeneration.value() > this.initialHullEHP.value()) {
				PositiveInteger hullRegenerationDiff = PositiveInteger
						.of(this.initialHullEHP.value() - this.hullEHP.value());
				this.defenciveSubsystem.setHullRegeneration(hullRegenerationDiff);
			}

			RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
			this.shieldEHP = PositiveInteger.of(this.shieldEHP.value() + regenerateAction.shieldHPRegenerated.value());
			this.hullEHP = PositiveInteger.of(this.hullEHP.value() + regenerateAction.hullHPRegenerated.value());
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			return Optional.of(regenerateAction);
		}
	}

}
