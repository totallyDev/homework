package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger powergridRequirments;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	private AttackSubsystemImpl() {
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		var attackSubsystemImpl = new AttackSubsystemImpl();
		attackSubsystemImpl.name = name;
		attackSubsystemImpl.powergridRequirments = powergridRequirments;
		attackSubsystemImpl.capacitorConsumption = capacitorConsumption;
		attackSubsystemImpl.optimalSpeed = optimalSpeed;
		attackSubsystemImpl.optimalSize = optimalSize;
		attackSubsystemImpl.baseDamage = baseDamage;

		return attackSubsystemImpl;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		var sizeReductionModifier = target.getSize().value() >= this.optimalSize.value() ? 1
				: (double) target.getSize().value() / this.optimalSize.value();
		var speedReductionModifier = target.getCurrentSpeed().value() <= this.optimalSpeed.value() ? 1
				: (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		return PositiveInteger
				.of((int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
