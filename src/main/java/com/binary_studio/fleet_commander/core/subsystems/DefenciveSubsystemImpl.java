package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger powergridConsumption;

	private PositiveInteger capacitorConsumption;

	private PositiveInteger impactReductionPercent;

	private PositiveInteger shieldRegeneration;

	private PositiveInteger hullRegeneration;

	private DefenciveSubsystemImpl() {
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name == null || name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		var defenciveSubsystemImpl = new DefenciveSubsystemImpl();
		defenciveSubsystemImpl.name = name;
		defenciveSubsystemImpl.powergridConsumption = powergridConsumption;
		defenciveSubsystemImpl.capacitorConsumption = capacitorConsumption;
		defenciveSubsystemImpl.impactReductionPercent = impactReductionPercent;
		defenciveSubsystemImpl.shieldRegeneration = shieldRegeneration;
		defenciveSubsystemImpl.hullRegeneration = hullRegeneration;

		return defenciveSubsystemImpl;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setShieldRegeneration(PositiveInteger shieldRegeneration) {
		this.shieldRegeneration = shieldRegeneration;
	}

	@Override
	public void setHullRegeneration(PositiveInteger hullRegeneration) {
		this.hullRegeneration = hullRegeneration;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		var reducedDamage = (int) Math.ceil(this.impactReductionPercent.value() <= 95
				? (1 - this.impactReductionPercent.value() * 0.01) * incomingDamage.damage.value()
				: 0.05 * incomingDamage.damage.value());
		return new AttackAction(PositiveInteger.of(reducedDamage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
