package com.binary_studio.academy_coin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		List<Integer> pricesList = prices.collect(Collectors.toList());
		long days = pricesList.size();
		int profit = 0;

		for (int i = 0; i < days - 1; i++) {
			if (pricesList.get(i) < pricesList.get(i + 1)) {
				int actual = pricesList.get(i + 1);
				int next = i + 2;

				while (next < days && actual <= pricesList.get(next)) {
					actual = pricesList.get(next);
					next++;
				}
				profit += actual - pricesList.get(i);
				i = next - 1;
			}
		}
		return profit;
	}

}
