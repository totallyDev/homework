package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.LinkedList;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {
		int librariesSize = libraries.libraries.size();
		ArrayList<LinkedList<Integer>> infoAboutDependency = new ArrayList<>(librariesSize);
		for (int i = 0; i < librariesSize; i++) {
			infoAboutDependency.add(new LinkedList<>());
		}

		for (String[] array : libraries.dependencies) {
			int start = libraries.libraries.indexOf(array[0]);
			int end = libraries.libraries.indexOf(array[1]);
			infoAboutDependency.get(start).add(end);
		}

		return true;
	}

}
